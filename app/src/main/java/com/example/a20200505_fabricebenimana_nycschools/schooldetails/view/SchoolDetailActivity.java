package com.example.a20200505_fabricebenimana_nycschools.schooldetails.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;

import com.example.a20200505_fabricebenimana_nycschools.databinding.ActivitySchoolDetailBinding;
import com.example.a20200505_fabricebenimana_nycschools.objectgraph.SchoolIdentification;
import com.example.a20200505_fabricebenimana_nycschools.objectgraph.ViewModelFactory;
import com.example.a20200505_fabricebenimana_nycschools.schooldetails.model.SchoolDetailViewModel;
import com.example.nyc_schools.network.model.SatScore;
import com.example.nyc_schools.network.model.School;
import com.example.nyc_schools.network.repository.Resource;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.lifecycle.ViewModelProvider;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjection;


public class SchoolDetailActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory mViewModelFactory;
    private ActivitySchoolDetailBinding binder;
    public static String KEY_SCHOOL = "School_key";

    @Module
    public static class ActivityModule {

        @Provides
        Activity provideActivity(SchoolDetailActivity activity) {
            return activity;
        }

        @Provides
        @SchoolIdentification
        String provideSchoolId(SchoolDetailActivity activity) {
            return ((School) Objects.requireNonNull(
                    Objects.requireNonNull(activity.getIntent().getExtras())
                            .get(KEY_SCHOOL))).getDbn();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binder = ActivitySchoolDetailBinding.inflate(getLayoutInflater());
        School school = (School) Objects.requireNonNull(getIntent().getExtras()).get(KEY_SCHOOL);
        if (school != null) {
            Objects.requireNonNull(getSupportActionBar()).setTitle(school.getSchool_name());
            SchoolDetailViewModel viewModel = new ViewModelProvider(this, mViewModelFactory).get(
                    SchoolDetailViewModel.class);
            renderSchoolInfo(school);
            viewModel.getScores().observe(this, this::handleState);
        }
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setContentView(binder.getRoot());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void renderSchoolInfo(School school) {
        binder.schoolName.setText(school.getSchool_name());
        binder.schoolDescription.setText(school.getOverview_paragraph());
        binder.phoneNumber.setText(
                Html.fromHtml(String.format("<b>Phone:</b> %s", school.getPhone_number()),
                        HtmlCompat.FROM_HTML_MODE_COMPACT));
        binder.fax.setText(Html.fromHtml(String.format("<b>Fax:</b> %s", school.getFax_number()),
                HtmlCompat.FROM_HTML_MODE_COMPACT));

        binder.email.setText(
                Html.fromHtml(String.format("<b>Email:</b>  <u>%s</u> ", school.getSchool_email()),
                        HtmlCompat.FROM_HTML_MODE_COMPACT));
        binder.website.setText(
                Html.fromHtml(String.format("<b>WWW:</b>  <u>%s</u> ", school.getWebsite()),
                        HtmlCompat.FROM_HTML_MODE_COMPACT));

        binder.opportunities.setText(Html.fromHtml(
                String.format("<b>Opportunities:</b> %s", school.getAcademicopportunities1()),
                HtmlCompat.FROM_HTML_MODE_COMPACT));

        binder.grades.setText(
                Html.fromHtml(String.format("<b>Grades:</b> %s", school.getFinalgrades()),
                        HtmlCompat.FROM_HTML_MODE_COMPACT));

        binder.students.setText(
                Html.fromHtml(String.format("<b>Students:</b> %s", school.getTotal_students()),
                        HtmlCompat.FROM_HTML_MODE_COMPACT));
    }


    private void handleState(Resource<List<SatScore>> state) {
        switch (state.getStatus()) {
            case ERROR:
            case SUCCESS:
                binder.satLoader.setVisibility(View.GONE);
                showSatAverageInfo(state.getData());
                break;
            case LOADING:
                binder.satLoader.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showSatAverageInfo(@Nullable List<SatScore> stores) {
        SatScore store = stores != null ? stores.get(0) : null;
        binder.satTakers.setText(Html.fromHtml(String.format("<b>SAT takers:</b> %s",
                store != null ? store.getNum_of_sat_test_takers() : "-"),
                HtmlCompat.FROM_HTML_MODE_COMPACT));
        binder.satMath.setText(Html.fromHtml(String.format("<b>Math:</b> %s",
                store != null ? store.getSat_math_avg_score() : "-"),
                HtmlCompat.FROM_HTML_MODE_COMPACT));

        binder.satWriting.setText(Html.fromHtml(String.format("<b>Writing:</b> %s",
                store != null ? store.getSat_writing_avg_score() : "-"),
                HtmlCompat.FROM_HTML_MODE_COMPACT));
        binder.satReading.setText(Html.fromHtml(String.format("<b>Reading:</b> %s",
                store != null ? store.getSat_critical_reading_avg_score() : "-"),
                HtmlCompat.FROM_HTML_MODE_COMPACT));
    }


    public static Intent getStartIntent(Context context, School school) {
        Intent intent = new Intent(context, SchoolDetailActivity.class);
        intent.putExtra(KEY_SCHOOL, school);
        return intent;
    }
}
