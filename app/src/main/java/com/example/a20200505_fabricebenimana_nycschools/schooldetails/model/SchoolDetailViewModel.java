package com.example.a20200505_fabricebenimana_nycschools.schooldetails.model;

import com.example.a20200505_fabricebenimana_nycschools.objectgraph.SchoolIdentification;
import com.example.nyc_schools.network.model.SatScore;
import com.example.nyc_schools.network.repository.Resource;
import com.example.nyc_schools.network.repository.SchoolRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;


public class SchoolDetailViewModel extends ViewModel {

    private LiveData<Resource<List<SatScore>>> mLiveData;

    @Inject
    SchoolDetailViewModel(SchoolRepository repo, @SchoolIdentification String schoolId) {
        mLiveData = repo.getScoresForSchool(schoolId);
    }

    public LiveData<Resource<List<SatScore>>> getScores() {
        return mLiveData;
    }
}
