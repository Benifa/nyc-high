package com.example.a20200505_fabricebenimana_nycschools.objectgraph.module


import com.example.a20200505_fabricebenimana_nycschools.schooldetails.view.SchoolDetailActivity
import com.example.a20200505_fabricebenimana_nycschools.schools.view.SchoolListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {
    @ContributesAndroidInjector(modules = [RepositoryModule::class, SchoolListViewModelModule::class])
    abstract fun contributeYourAndroidInjector(): SchoolListActivity

    @ContributesAndroidInjector(modules = [RepositoryModule::class, SchoolDetailViewModelModule::class,
        SchoolDetailActivity.ActivityModule::class])
    abstract fun contributeYourAndroidInjector2(): SchoolDetailActivity
}
