package com.example.a20200505_fabricebenimana_nycschools.objectgraph

import com.example.a20200505_fabricebenimana_nycschools.FabNYCSchoolApplication
import com.example.a20200505_fabricebenimana_nycschools.objectgraph.module.BuildersModule
import com.example.a20200505_fabricebenimana_nycschools.schooldetails.view.SchoolDetailActivity
import dagger.Component
import dagger.android.AndroidInjectionModule


@Component(modules = [AndroidInjectionModule::class, BuildersModule::class,  SchoolDetailActivity.ActivityModule::class])
interface ApplicationComponent {
    fun inject(application: FabNYCSchoolApplication)
}