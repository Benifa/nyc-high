package com.example.a20200505_fabricebenimana_nycschools.objectgraph.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20200505_fabricebenimana_nycschools.objectgraph.ViewModelFactory
import com.example.a20200505_fabricebenimana_nycschools.objectgraph.ViewModelKey
import com.example.a20200505_fabricebenimana_nycschools.schooldetails.model.SchoolDetailViewModel
import com.example.a20200505_fabricebenimana_nycschools.schools.model.SchoolsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SchoolListViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider
    .Factory

    @Binds
    @IntoMap
    @ViewModelKey(SchoolsViewModel::class)
    internal abstract fun bindSchoolListViewModel(viewModel: SchoolsViewModel): ViewModel
}

@Module
abstract class SchoolDetailViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider
    .Factory

    @Binds
    @IntoMap
    @ViewModelKey(SchoolDetailViewModel::class)
    internal abstract fun bindSchoolDetailViewModel(viewModel: SchoolDetailViewModel): ViewModel
}


