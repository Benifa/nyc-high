package com.example.a20200505_fabricebenimana_nycschools.objectgraph.module

import com.example.nyc_schools.network.repository.NYCSchoolRepository
import com.example.nyc_schools.network.repository.SchoolRepository
import com.example.nyc_schools.network.service.NYCSchoolsService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
object RepositoryModule {
    @Provides
    @JvmStatic
    fun paidOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
        return builder.build()
    }

    @Provides
    @JvmStatic
    fun provideGoogleApiRetrofit(client: OkHttpClient): Retrofit {
        val baseUrl = "https://data.cityofnewyork.us"
        return Retrofit.Builder().baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @JvmStatic
    fun provideGoogleApisService(retrofit: Retrofit): NYCSchoolsService =
        retrofit.create(NYCSchoolsService::class.java)

    @Provides
    @JvmStatic
    fun provideRepository(repo: NYCSchoolRepository): SchoolRepository = repo


}