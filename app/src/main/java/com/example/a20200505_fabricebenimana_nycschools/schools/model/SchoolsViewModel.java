package com.example.a20200505_fabricebenimana_nycschools.schools.model;

import com.example.nyc_schools.network.model.School;
import com.example.nyc_schools.network.repository.Resource;
import com.example.nyc_schools.network.repository.SchoolRepository;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;


public class SchoolsViewModel extends ViewModel {

    @Inject
    SchoolsViewModel(SchoolRepository repo) {
        mLiveData = repo.fetchAllSchools();
    }

    private LiveData<Resource<List<School>>> mLiveData;

    public LiveData<Resource<List<School>>> getSchools() {
        return mLiveData;
    }
}
