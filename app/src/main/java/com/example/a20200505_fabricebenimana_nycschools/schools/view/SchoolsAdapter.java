package com.example.a20200505_fabricebenimana_nycschools.schools.view;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.a20200505_fabricebenimana_nycschools.databinding.SchoolItemBinding;
import com.example.a20200505_fabricebenimana_nycschools.schooldetails.view.SchoolDetailActivity;
import com.example.nyc_schools.network.model.School;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;


class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolHolder> {

    private List<School> items;
    private List<School> fullSchoolList;
    private LayoutInflater mLayoutInflater;


    SchoolsAdapter(List<School> schools, LayoutInflater layoutInflater) {
        this.items = schools;
        this.fullSchoolList = new ArrayList<>(schools);
        mLayoutInflater = layoutInflater;
    }


    @NonNull
    @Override
    public SchoolsAdapter.SchoolHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SchoolHolder(SchoolItemBinding.inflate(mLayoutInflater));
    }


    @Override
    public void onBindViewHolder(SchoolsAdapter.SchoolHolder holder, int position) {
        School school = items.get(position);
        renderSchoolInfo(holder, school);
        Context context = holder.itemView.getContext();
        holder.itemView.setOnClickListener(v -> {
            context.startActivity(SchoolDetailActivity.getStartIntent(context, school));
        });
    }


    private void renderSchoolInfo(SchoolHolder holder, School school) {
        holder.binding.schoolName.setText(school.getSchool_name());
        holder.binding.schoolDescription.setText(school.getOverview_paragraph());
        holder.binding.location.setText(school.getNeighborhood());
        holder.binding.focus.setText(
                fromHtml(String.format("<b>Focus:</b> %s", school.getInterest1())));
        holder.binding.grades.setText(
                fromHtml(String.format("<b>Grades:</b> %s", school.getFinalgrades())));
        holder.binding.totalStudents.setText(fromHtml(
                String.format("<b>Number of Students:</b> %s", school.getTotal_students())));
    }


    private Spanned fromHtml(String HtmlString) {
        return Html.fromHtml(HtmlString, HtmlCompat.FROM_HTML_MODE_COMPACT);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SchoolHolder extends RecyclerView.ViewHolder {

        SchoolItemBinding binding;

        SchoolHolder(SchoolItemBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }

    void filter(String queryText) {
        items.clear();
        if (queryText.isEmpty()) {
            items.addAll(fullSchoolList);
        } else {
            items = fullSchoolList.stream().filter(school -> school.getSchool_name().toLowerCase().contains(queryText.toLowerCase())).collect(
                            Collectors.toList());
        }
        notifyDataSetChanged();
    }
}
