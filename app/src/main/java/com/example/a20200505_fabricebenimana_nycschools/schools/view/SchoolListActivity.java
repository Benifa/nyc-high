package com.example.a20200505_fabricebenimana_nycschools.schools.view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.a20200505_fabricebenimana_nycschools.R;
import com.example.a20200505_fabricebenimana_nycschools.databinding.ActivitySchoolListBinding;
import com.example.a20200505_fabricebenimana_nycschools.schools.model.SchoolsViewModel;
import com.example.nyc_schools.network.model.School;
import com.example.nyc_schools.network.repository.Resource;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.AndroidInjection;


public class SchoolListActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ActivitySchoolListBinding binder;
    private SchoolsAdapter mSchoolsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binder = ActivitySchoolListBinding.inflate(getLayoutInflater());
        SchoolsViewModel mSchoolsViewModel = new ViewModelProvider(this, mViewModelFactory).get(SchoolsViewModel.class);
        mSchoolsViewModel.getSchools().observe(this, this::handleState);
        setContentView(binder.getRoot());
    }

    private void handleState(Resource<List<School>> state) {
        switch (state.getStatus()) {
            case SUCCESS:
                mSchoolsAdapter = new SchoolsAdapter(state.getData(), getLayoutInflater());
                binder.schools.setLayoutManager(new LinearLayoutManager(this));
                binder.schools.addItemDecoration(new ItemOffsetDecoration(16));
                binder.schools.setAdapter(mSchoolsAdapter);
                binder.loader.setVisibility(View.GONE);
                binder.schools.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                binder.loader.setVisibility(View.GONE);

            case LOADING:
                binder.loader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        mSchoolsAdapter.filter(newText);
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem searchItem = menu.findItem(R.id.searchBar);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.clearFocus();
        searchView.setQueryHint("Search School");
        searchView.setOnQueryTextListener(this);
        return true;
    }
}
