package com.example.nyc_schools.network.repository;

import com.example.nyc_schools.network.api.NYCSchoolsApi;
import com.example.nyc_schools.network.model.School;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;


public class NYCSchoolRepositoryTest {

    @Mock
    NYCSchoolsApi networkApi;

    @Mock
    Call<List<School>> schoolsCall;

    @Mock
    List<School> mSchools;

    @Mock
    ResponseBody mResponseBody;


    @InjectMocks
    NYCSchoolRepository mRepository;

    private LiveData<Resource<List<School>>> liveData;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(networkApi.getHighSchools()).thenReturn(schoolsCall);
    }


    @Test
    public void shouldPostLoadingStateAtTheBeginning() {
        whenUserFetchSchools();
        verifyLoadingStateIsPosted();
    }

    @Test
    public void shouldPostOnErrorResultWhenNetworkCallFailsCompletely() {
        givenNetworkCallFail();
        whenUserFetchSchools();
        verifyErrorStateIsPosted();
    }

    @Test
    public void shouldPostOnErrorResultWhenNetworkCallReturnsErrorResponse() {
        givenErrorSchoolResponse();
        whenUserFetchSchools();
        verifyErrorStateIsPosted();
    }

    @Test
    public void shouldPostOnErrorResultWhenNetworkCallReturnsNoContentResponse_204() {
        givenNoContentSchoolResponse();
        whenUserFetchSchools();
        verifyErrorStateIsPosted();
    }

    @Test
    public void shouldPostOnErrorResultWhenSchoolListIsEmpty() {
        givenEmptySchoolResponse();
        whenUserFetchSchools();
        verifyErrorStateIsPosted();
    }

    @Test
    public void shouldPostSuccessResultWhenNetworkCallReturnsNonEmptyList() {
        givenNonEmptySchoolResponse();
        whenUserFetchSchools();
        verifySuccessStateIsPosted();
    }

    private void verifyLoadingStateIsPosted() {
        assertThat(liveData).isNotNull();
        assertThat(liveData.getValue()).isInstanceOf(Resource.class);
        Resource<List<School>> resource = liveData.getValue();
        assertThat(resource.getStatus()).isEqualTo(LoadingState.LOADING);
    }

    private void verifySuccessStateIsPosted() {
        assertThat(liveData).isNotNull();
        assertThat(liveData.getValue()).isInstanceOf(Resource.class);
        Resource<List<School>> resource = liveData.getValue();
        assertThat(resource.getStatus()).isEqualTo(LoadingState.SUCCESS);
    }

    private void verifyErrorStateIsPosted() {
        assertThat(liveData).isNotNull();
        assertThat(liveData.getValue()).isInstanceOf(Resource.class);
        Resource<List<School>> resource = liveData.getValue();
        assertThat(resource.getStatus()).isEqualTo(LoadingState.ERROR);
    }


    private void whenUserFetchSchools() {
        liveData = mRepository.fetchAllSchools();
    }


    private void givenNetworkCallFail() {
        Mockito.doAnswer(invocation -> {
            Callback<List<School>> schoolCallback = invocation.getArgument(0, Callback.class);
            schoolCallback.onFailure(schoolsCall, new Throwable("Error"));
            return null;
        }).when(schoolsCall).enqueue(any(Callback.class));
    }

    private void givenEmptySchoolResponse() {
        Mockito.doAnswer(invocation -> {
            Callback<List<School>> schoolCallback = invocation.getArgument(0, Callback.class);
            schoolCallback.onResponse(schoolsCall, Response.success(200, mSchools));
            return null;
        }).when(schoolsCall).enqueue(any(Callback.class));
    }

    private void givenNonEmptySchoolResponse() {
        Mockito.doAnswer(invocation -> {
            Mockito.when(mSchools.size()).thenReturn(2);
            Callback<List<School>> schoolCallback = invocation.getArgument(0, Callback.class);
            schoolCallback.onResponse(schoolsCall, Response.success(200, mSchools));
            return null;
        }).when(schoolsCall).enqueue(any(Callback.class));
    }


    private void givenNoContentSchoolResponse() {
        Mockito.doAnswer(invocation -> {
            Callback<List<School>> schoolCallback = invocation.getArgument(0, Callback.class);
            schoolCallback.onResponse(schoolsCall, Response.success(204, mSchools));
            return null;
        }).when(schoolsCall).enqueue(any(Callback.class));
    }

    private void givenErrorSchoolResponse() {
        Mockito.doAnswer(invocation -> {
            Callback<List<School>> schoolCallback = invocation.getArgument(0, Callback.class);
            schoolCallback.onResponse(schoolsCall, Response.error(400, mResponseBody));
            return null;
        }).when(schoolsCall).enqueue(any(Callback.class));
    }
}