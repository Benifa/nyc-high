package com.example.nyc_schools.network.repository;

import com.example.nyc_schools.network.api.NYCSchoolsApi;
import com.example.nyc_schools.network.model.SatScore;
import com.example.nyc_schools.network.model.School;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NYCSchoolRepository implements SchoolRepository {

    private NYCSchoolsApi networkApi;


    @Inject
    public NYCSchoolRepository(NYCSchoolsApi api) {
        networkApi = api;
    }


    @Override
    public LiveData<Resource<List<School>>> fetchAllSchools() {
        MutableLiveData<Resource<List<School>>> schools = new MutableLiveData<>();
        schools.postValue(Resource.loading());
        networkApi.getHighSchools().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(@NotNull Call<List<School>> call,
                    @NotNull Response<List<School>> response) {
                if (response.isSuccessful() && response.body() !=null && response.body().size() > 0) {
                    schools.postValue(Resource.success(response.body()));
                } else {
                    schools.postValue(Resource.error(response.message()));
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<School>> call, @NotNull Throwable t) {
                schools.postValue(Resource.error(Objects.requireNonNull(t.getMessage())));
            }
        });
        return schools;
    }


    @Override
    public LiveData<Resource<List<SatScore>>> getScoresForSchool(String schoolId) {
        MutableLiveData<Resource<List<SatScore>>> satScores = new MutableLiveData<>();
        satScores.postValue(Resource.loading());
        networkApi.getSATScoresForSchool(schoolId).enqueue(new Callback<List<SatScore>>() {
            @Override
            public void onResponse(@NotNull Call<List<SatScore>> call, @NotNull Response<List<SatScore>> response) {
                if (response.isSuccessful() && Objects.requireNonNull(response.body()).size() > 0) {
                    satScores.postValue(Resource.success(response.body()));
                } else {
                    satScores.postValue(Resource.error(response.message()));
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<SatScore>> call, @NotNull Throwable t) {
                satScores.postValue(Resource.error(Objects.requireNonNull(t.getMessage())));
            }
        });
        return satScores;
    }

}
