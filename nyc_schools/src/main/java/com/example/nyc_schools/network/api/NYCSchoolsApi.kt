package com.example.nyc_schools.network.api

import com.example.nyc_schools.network.model.SatScore
import com.example.nyc_schools.network.model.School
import com.example.nyc_schools.network.service.NYCSchoolsService
import retrofit2.Call
import javax.inject.Inject

class NYCSchoolsApi @Inject constructor(private val nycSchoolsService: NYCSchoolsService) {
    fun getHighSchools(): Call<List<School>> = nycSchoolsService.getSchools()
    fun getSATScoresForSchool(schoolId: String): Call<List<SatScore>> = nycSchoolsService.getScoresForSchool(schoolId)
}