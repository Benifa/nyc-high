package com.example.nyc_schools.network.repository

data class Resource<out T>(val status: LoadingState, val data: T?, val message: String?) {
    companion object {
        @JvmStatic
        fun <T> success(data: T?): Resource<T> {
            return Resource(LoadingState.SUCCESS, data, null)
        }
        @JvmStatic
        fun <T> error(msg: String): Resource<T> {
            return Resource(LoadingState.ERROR, null, msg)
        }
        @JvmStatic
        fun <T> loading(): Resource<T> {
            return Resource(LoadingState.LOADING, null, null)
        }
    }
}

 enum class LoadingState {
    SUCCESS, ERROR, LOADING
}

