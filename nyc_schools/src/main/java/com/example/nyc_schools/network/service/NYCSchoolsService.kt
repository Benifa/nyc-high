package com.example.nyc_schools.network.service


import com.example.nyc_schools.network.model.SatScore
import com.example.nyc_schools.network.model.School
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface NYCSchoolsService {
    @GET(SAT)
    fun getScoresForSchool(@Query("dbn") schoolId: String): Call<List<SatScore>>

    @GET(SCHOOL_LIST)
    fun getSchools(): Call<List<School>>

    companion object {
        private const val CITY_RESOURCE = "/resource"
        private const val SCHOOL_LIST =
            "$CITY_RESOURCE/s3k6-pzi2.json"

        private const val SAT = "$CITY_RESOURCE/f9bf-2cp4.json"
    }
}