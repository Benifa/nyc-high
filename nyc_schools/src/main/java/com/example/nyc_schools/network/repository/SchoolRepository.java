package com.example.nyc_schools.network.repository;

import com.example.nyc_schools.network.model.SatScore;
import com.example.nyc_schools.network.model.School;

import java.util.List;

import androidx.lifecycle.LiveData;


public interface SchoolRepository {
    /**
     * Fetch Schools List
     * @return livedata
     */
    LiveData<Resource<List<School>>> fetchAllSchools();

    /**
     * Fetch SAT scores for a Given school
     * @param  schoolId school identification
     * @return livedata
     */
    LiveData<Resource<List<SatScore>>> getScoresForSchool(String schoolId);
}
