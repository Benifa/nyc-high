package com.example.nyc_schools.network.model


import android.annotation.SuppressLint
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class SatScore(
    @Json(name = "dbn") val dbn: String,
    @Json(name = "num_of_sat_test_takers") val num_of_sat_test_takers: String,
    @Json(name = "sat_critical_reading_avg_score") val sat_critical_reading_avg_score: String,
    @Json(name = "sat_math_avg_score") val sat_math_avg_score: String,
    @Json(name = "sat_writing_avg_score") val sat_writing_avg_score: String,
    @Json(name = "school_name") val school_name: String
) : Parcelable